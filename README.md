# Merge Request diff count does not match the displayed changes

[MR displaying the problem](https://gitlab.com/gerardo/merge-request-diff-count-does-not-match-the-displayed-changes/-/merge_requests/2/diffs_)

## Steps to reproduce
``` bash
# git remote add origin REMOTE_URL
echo "/main.txt @user" > CODEOWNERS
git add CODEOWNERS
git commit -m "Inital commit with CODEOWNERS file"

# Have a lower env branch
git checkout -b "dev"
echo "dev" > dev.txt
git add dev.txt
git commit -m "Commit to dev"

# Represent that something is going on in main branch
git checkout main
echo "main" > main.txt
git add main.txt
git commit -m "Commit to main"

# Merge main in dev
git checkout dev
git merge main --no-edit

# Start Feature 1
git checkout $(git log --oneline | tail -1 | awk '{print $1}')
git checkout -b feature1
echo "feature1.txt" > feature1.txt
git add feature1.txt
git commit -m "Feature1"

# Start Feature 2 
git checkout $(git log --oneline | tail -1 | awk '{print $1}')
git checkout -b feature2
echo "feature2.txt" > feature2.txt
git add feature2.txt
git commit -m "Feature2"

# Feature 1 finalize and merge to dev and main
git checkout dev
git merge feature1 --no-edit
git checkout main
git merge feature1 --no-edit

# Merge main into Feature2
git checkout feature2
git merge main --no-edit
# Open MR in Gitlab Feature2 to dev
git push -u --force origin main dev feature1 feature2
```
